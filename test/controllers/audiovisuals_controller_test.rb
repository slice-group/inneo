require 'test_helper'

class AudiovisualsControllerTest < ActionController::TestCase
  setup do
    @audiovisual = audiovisuals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:audiovisuals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create audiovisual" do
    assert_difference('Audiovisual.count') do
      post :create, audiovisual: {  }
    end

    assert_redirected_to audiovisual_path(assigns(:audiovisual))
  end

  test "should show audiovisual" do
    get :show, id: @audiovisual
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @audiovisual
    assert_response :success
  end

  test "should update audiovisual" do
    patch :update, id: @audiovisual, audiovisual: {  }
    assert_redirected_to audiovisual_path(assigns(:audiovisual))
  end

  test "should destroy audiovisual" do
    assert_difference('Audiovisual.count', -1) do
      delete :destroy, id: @audiovisual
    end

    assert_redirected_to audiovisuals_path
  end
end
