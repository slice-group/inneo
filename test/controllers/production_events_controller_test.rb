require 'test_helper'

class ProductionEventsControllerTest < ActionController::TestCase
  setup do
    @production_event = production_events(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:production_events)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create production_event" do
    assert_difference('ProductionEvent.count') do
      post :create, production_event: {  }
    end

    assert_redirected_to production_event_path(assigns(:production_event))
  end

  test "should show production_event" do
    get :show, id: @production_event
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @production_event
    assert_response :success
  end

  test "should update production_event" do
    patch :update, id: @production_event, production_event: {  }
    assert_redirected_to production_event_path(assigns(:production_event))
  end

  test "should destroy production_event" do
    assert_difference('ProductionEvent.count', -1) do
      delete :destroy, id: @production_event
    end

    assert_redirected_to production_events_path
  end
end
