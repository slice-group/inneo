function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}


$( document ).ready(function() {
  function scrollToAnchor(id){
    var aTag = $("#"+id);
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
  }
  
  $(".scroll-down").on('click', function() {
    scrollToAnchor("services");
  });


  document.onscroll = function() {
    if( $(window).scrollTop() > $('#home').height() ) {
        $('.navbar').removeClass('navbar-static-top').addClass('navbar-fixed-top');
        $(".titles-inneo").css("padding-top", "120px");
    }
    else {
        $('.navbar').removeClass('navbar-fixed-top').addClass('navbar-static-top');
        $(".titles-inneo").css("padding-top", "50px");
    }
  };

  $('.navbar-nav').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 500,
    scrollThreshold: 0.5,
    filter: ':not(.external)',
    easing: 'swing',
    begin: function() {
      removeHash();
    },
    end: function() {
      $('.navbar-collapse').collapse('hide');
    },

    scrollChange: function($currentListItem) {}
    
  });
  
});