require 'elasticsearch/model'
class ProductionEvent < ActiveRecord::Base
	include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
	validates_presence_of :name, :email, :telephone, :event_type, :number_guests, :hour_begin, :hour_end, :mode, :date_event
	validate :validates_fields
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "Formato inválido" }
  	validates :telephone, :format => { :with => /\d{11}/, :on => :create, :message => "Formato inválido" }
  	before_save :change_values_other_fields
	attr_accessor :other_event_type, :suggest, :other_service_check, :other_group_check


	def change_values_other_fields
		self.place = "SUGERIR" if self.suggest == "1" 
		self.other_group = "" if self.other_group_check.nil?
		self.other_service = "" if self.other_service_check.nil?
		self.event_type = self.other_event_type if self.event_type == "OTRO"
	end

	def validates_fields
		errors.add(:errors, "Debe indicar el tipo de evento") if (self.event_type == "OTRO" and self.other_event_type == "")
		errors.add(:errors, "Debe indicar el servicio adicional") if (self.other_service_check == "1" and self.other_service == "")
		errors.add(:errors, "Debe indicar el grupo") if (self.other_group_check == "1" and self.other_group == "")
		errors.add(:errors, "Debe indicar el lugar del evento") if (self.place == "" and self.suggest.nil?)
		errors.add(:errors, "Debe indicar la hora de Inicio" ) if self.hour_begin == "HORA DE INICIO"
		errors.add(:errors, "Debe indicar la hora de Culminación" ) if self.hour_end == "HORA DE CULMINACIÓN"
		errors.add(:errors, "La fecha del Evento es inválida") if self.date_event < Date.today
		errors.add(:errors, "La cantidad de invitados es inválida") if self.number_guests.to_i < 1
	end

	def self.index(current_user)
    ProductionEvent.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.count
  end

  def self.route_index
    "/admin/production_events"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    ProductionEvent.destroy ids
  end
end
