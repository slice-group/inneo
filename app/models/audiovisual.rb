require 'elasticsearch/model'
class Audiovisual < ActiveRecord::Base

	include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  validates_presence_of :name, :email, :telephone, :brand
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "Formato inválido" }
  validates :telephone, :format => { :with => /\d{11}/, :on => :create, :message => "Formato inválido" }
  validate :validates_fields
  before_save :change_values_other_fields
  attr_accessor :photo_print_check,:other_photo_check,:other_animation_check,:other_marketing_check,:other_other_check,:animation_2d_check,:animation_3d_check, :radio_jingle_check,:shortfilm_check,:tv_spot_check,:web_spot_check,:video_report_check,:videoclip_check,:video_bio_check,:loop_screens_check,:wedding_video_check,:fifteen_video_check,:corp_event_check,:documentary_check,:promo_check,:other_video_check


  def change_values_other_fields
    self.photo_print = nil if self.photo_print_check.nil?
    self.other_photo = nil if self.other_photo_check.nil?
    self.other_animation = nil if self.other_animation_check.nil?
    self.other_marketing = nil if self.other_marketing_check.nil?
    self.other_other = nil if self.other_other_check.nil?
    self.animation_2d = nil if self.animation_2d_check.nil?
    self.animation_3d = nil if self.animation_3d_check.nil?
    self.shortfilm = nil if self.shortfilm_check.nil?
    self.tv_spot = nil if self.tv_spot_check.nil?
    self.web_spot = nil if self.web_spot_check.nil?
    self.video_report = nil if self.video_report_check.nil?
    self.videoclip = nil if self.videoclip_check.nil?
    self.video_bio = nil if self.video_bio_check.nil?
    self.loop_screens = nil if self.loop_screens_check.nil?
    self.wedding_video = nil if self.wedding_video_check.nil?
    self.fifteen_video = nil if self.fifteen_video_check.nil?
    self.corp_event = nil if self.corp_event_check.nil?
    self.documentary = nil if self.documentary_check.nil?
    self.promo = nil if self.promo_check.nil?
    self.radio_jingle = nil if self.radio_jingle_check.nil?
    self.other_video = nil if self.other_video_check.nil?
  end

  def validates_fields
    errors.add(:errors, "Debe indicar el servicio adicional de Fotografía.") if ( self.other_photo_check == "1" and self.other_photo == "")
    errors.add(:errors, "Debe indicar el servicio adicional de Motion Graphics.") if ( self.other_animation_check == "1" and self.other_animation == "")
    errors.add(:errors, "Debe indicar el servicio adicional de Marketing.") if ( self.other_marketing_check == "1" and self.other_marketing == "")
    errors.add(:errors, "Debe indicar el servicio adicional de Otros.") if ( self.other_other_check == "1" and self.other_other == "")
    errors.add(:errors, "Debe indicar el servicio adicional de Video.") if ( self.other_video_check == "1" and self.other_video == "")
    errors.add(:errors, "Debe indicar la cantidad de Impresiones de Fotos 8x10.") if ( self.photo_print_check == "1" and self.photo_print == "")
    errors.add(:errors, "Debe indicar la duración la Animación 2D.") if ( self.animation_2d_check == "1" and self.animation_2d == "")
    errors.add(:errors, "Debe indicar la duración la Animación 3D.") if ( self.animation_3d_check == "1" and self.animation_3d == "")
    errors.add(:errors, "Debe indicar la duración del Shortfilm.") if ( self.shortfilm_check == "1" and self.shortfilm == "")
    errors.add(:errors, "Debe indicar la duración del Spot TV.") if ( self.tv_spot_check == "1" and self.tv_spot == "")
    errors.add(:errors, "Debe indicar la duración del Spot Web.") if ( self.web_spot_check == "1" and self.web_spot == "")
    errors.add(:errors, "Debe indicar la duración del Video Reportaje.") if ( self.video_report_check == "1" and self.video_report == "")
    errors.add(:errors, "Debe indicar la duración del VideoClip.") if ( self.videoclip_check == "1" and self.videoclip == "")
    errors.add(:errors, "Debe indicar la duración de la Video Biografía.") if ( self.video_bio_check == "1" and self.video_bio == "")
    errors.add(:errors, "Debe indicar la duración de Loop Screens.") if ( self.loop_screens_check == "1" and self.loop_screens == "")
    errors.add(:errors, "Debe indicar la duración de Video de Boda.") if ( self.wedding_video_check == "1" and self.wedding_video == "")
    errors.add(:errors, "Debe indicar la duración de Video de 15 Años.") if ( self.fifteen_video_check == "1" and self.fifteen_video == "")
    errors.add(:errors, "Debe indicar la duración del Video de Evento COrporativo.") if ( self.corp_event_check == "1" and self.corp_event == "")
    errors.add(:errors, "Debe indicar la duración del Documental.") if ( self.documentary_check == "1" and self.documentary == "")
    errors.add(:errors, "Debe indicar la duración del Promocional Publicitario.") if ( self.promo_check == "1" and self.promo == "")
    errors.add(:errors, "Debe indicar la duración del Jingle Radial.") if ( self.radio_jingle_check == "1" and self.radio_jingle == "")
    errors.add(:errors, "Dato inválido en impresión de fotos 8x10.") if ( self.photo_print_check == "1" and self.photo_print.to_i < 1)
  end

  def self.index(current_user)
    Audiovisual.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.count
  end

  def self.route_index
    "/admin/audiovisuals"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    Audiovisual.destroy ids
  end

end
