require 'elasticsearch/model'
class Web < ActiveRecord::Base
	include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  validates_presence_of :name, :email, :telephone, :brand, :brand_description, :brand_target
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "Formato inválido" }
  validates :telephone, :format => { :with => /\d{11}/, :on => :create, :message => "Formato inválido" }
  validate :validates_fields
  before_save :change_values_other_fields
  attr_accessor :other_content_check


  def change_values_other_fields
    self.other_content = "" if self.other_content_check.nil?
    self.photo = false if self.audio_video_hq.nil?
    self.video = false if self.audio_video_hq.nil?
    self.audio = false if self.audio_video_hq.nil?
    self.animation = false if self.audio_video_hq.nil?
    self.socials = false if self.audio_video_hq.nil?
    self.other_content = nil if self.audio_video_hq.nil?
  end

  def validates_fields
    errors.add(:errors, "Debe indicar el servicio adicional") if (self.other_content_check == "1" and self.other_content == "")
    errors.add(:errors, "Las direcciones señaladas son iguales") if ((self.url1 != "" and self.url2 != "") and (self.url1 == self.url2))
    errors.add(:errors, "Las direcciones señaladas son iguales") if ((self.url1 != "" and self.url3 != "") and (self.url1 == self.url3))
    errors.add(:errors, "Las direcciones señaladas son iguales") if ((self.url2 != "" and self.url3!= "") and (self.url2 == self.url3))
  end

  def self.index(current_user)
    Web.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.count
  end

  def self.route_index
    "/admin/webs"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    Web.destroy ids
  end

end
