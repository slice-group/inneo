require 'elasticsearch/model'
class Brand < ActiveRecord::Base

  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  validates_presence_of :name, :email, :telephone, :brand_name
  validate :validates_fields
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "Formato inválido" }
  validates :telephone, :format => { :with => /\d{11}/, :on => :create, :message => "Formato inválido" }
  before_save :change_values_other_fields
  attr_accessor :other_corp_check, :other_pop_check, :backing_check, :backing_height, :backing_width, :other_printed_check


  def change_values_other_fields
    self.other_corp = "" if self.other_corp_check.nil?
    self.other_pop = "" if self.other_pop_check.nil?
    self.other_printed = "" if self.other_printed_check.nil?
    self.backing = "#{self.backing_height} x #{self.backing_width}" if self.backing_check == "1"
  end

  def validates_fields
    errors.add(:errors, "Debe indicar el servicio adicional de Identidad Corporativa") if (self.other_corp_check == "1" and self.other_corp == "")
    errors.add(:errors, "Debe indicar el servicio adicional de Material P.O.P") if (self.other_pop_check == "1" and self.other_pop == "")
    errors.add(:errors, "Debe indicar el servicio adicional de Material Impreso") if (self.other_printed_check == "1" and self.other_printed == "")
    errors.add(:errors, "Debe indicar las dimensiones del Backing") if (self.backing_check == "1" and ((self.backing_height == "" or self.backing_width == "") or (self.backing_width.to_i <1 or self.backing_height.to_i <1)))
  end

  def self.index(current_user)
    Brand.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.count
  end

  def self.route_index
    "/admin/brands"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    Brand.destroy ids
  end

end
