class ProductionEventsController < ApplicationController
  layout :resolve_layout
  before_action :set_production_event, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:new,:create]


  # GET /production_events/1
  # GET /production_events/1.json
  def show
    @event = ProductionEvent.find(params[:id])
  end

  # GET /production_events/new
  def new
    @production_event = ProductionEvent.new
  end

  # GET /production_events/1/edit
  def edit
    @event = ProductionEvent.find(params[:id])
  end

  # POST /production_events
  # POST /production_events.json
  def create
    @production_event = ProductionEvent.new(production_event_params)

    respond_to do |format|
      if @production_event.save
        format.html { redirect_to '/presupuestos', notice: 'SU SOLICITUD PARA PRESUPUESTO DE PRODUCCIÓN DE EVENTOS/DIVOZ HA SIDO ENVIADA.' }
        format.json { render :show, status: :created, location: @production_event }
        BudgetMailer.budget_email(production_event_params, "Producción de Eventos", "http://localhost:3000/admin/production_events/#{ProductionEvent.last.id}").deliver_now
      else
        format.html { render :new }
        format.json { render json: @production_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /production_events/1
  # PATCH/PUT /production_events/1.json
  def update
    respond_to do |format|
      if @production_event.update(production_event_params)
        format.html { redirect_to @production_event, notice: 'Production event was successfully updated.' }
        format.json { render :show, status: :ok, location: @production_event }
      else
        format.html { render :edit }
        format.json { render json: @production_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /production_events/1
  # DELETE /production_events/1.json
  def destroy
    @production_event.destroy
    respond_to do |format|
      format.html { redirect_to production_events_url, notice: 'Production event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_production_event
      @production_event = ProductionEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def production_event_params
      params.require(:production_event).permit(:date_event, :name, :email, :telephone, :event_type, :place, :number_guests, :hour_begin, :hour_end, :mode, :sound, :video, :confetti, :lights, :sparkles, :led_robot, :led_screens, :fireworks, :strippers, :photoshoot, :garotas, :stilts, :other_service, :strings, :drums, :mariachi, :vallenato, :other_group, :comment, :other_event_type, :suggest, :other_service_check, :other_group_check)
    end

    def resolve_layout
      case action_name
      when "destroy", "update", "edit", "show", "index"
        'admin/application'
      else
        'frontend/application'
      end
      end
end
