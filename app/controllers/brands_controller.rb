class BrandsController < ApplicationController
  before_action :set_brand, only: [:show, :edit, :update, :destroy]
  layout :resolve_layout
  before_filter :authenticate_user!, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:new,:create]

  # GET /brands
  # GET /brands.json


  # GET /brands/1
  # GET /brands/1.json
  def show
    @brand = Brand.find(params[:id])
  end

  # GET /brands/new
  def new
    @brand = Brand.new
  end

  # GET /brands/1/edit
  def edit
    @brand = Brand.find(params[:id])
  end

  # POST /brands
  # POST /brands.json
  def create
    @brand = Brand.new(brand_params)

    respond_to do |format|
      if @brand.save
        format.html { redirect_to '/presupuestos', notice: 'SU SOLICITUD PARA PRESUPUESTO DE BRANDING HA SIDO ENVIADA.' }
        format.json { render :show, status: :created, location: @brand }
        BudgetMailer.budget_email(brand_params, "Branding", "http://localhost:3000/admin/brands/#{Brand.last.id}").deliver_now
      else
        format.html { render :new }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /brands/1
  # PATCH/PUT /brands/1.json
  def update
    respond_to do |format|
      if @brand.update(brand_params)
        format.html { redirect_to @brand, notice: 'Brand was successfully updated.' }
        format.json { render :show, status: :ok, location: @brand }
      else
        format.html { render :edit }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    @brand.destroy
    respond_to do |format|
      format.html { redirect_to "/admin/brands", notice: 'Brand was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand = Brand.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def brand_params
      params.require(:brand).permit( :name, :email, :telephone, :brand_name, :logo, :tdp, :envelope, :folder, :email_signature, :products_menu, :brand_menu, :letterhead, :notebook, :carnet, :bill, :uniform, :brochure, :other_corp, :pens, :calendars, :mouse_pad, :disposable_ids, :resin_ids, :mugs, :coolers, :key_chain, :hats, :t_shirts, :stickers, :other_pop, :poster_half_a4, :poster_quarter_a4, :poster, :press_full,:press_double, :press_half, :press_quarter, :press_eights, :pennon, :banner, :backing, :other_printed , :other_corp_check, :other_pop_check, :backing_check, :backing_height, :backing_width, :other_printed_check)
    end

    def resolve_layout
      case action_name
      when "destroy", "update", "edit", "show", "index"
        'admin/application'
      else
        'frontend/application'
      end
    end
end
