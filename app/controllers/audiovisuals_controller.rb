class AudiovisualsController < ApplicationController
  before_action :set_audiovisual, only: [:show, :edit, :update, :destroy]
  layout :resolve_layout
  before_filter :authenticate_user!, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:new,:create]

  # GET /audiovisuals/1
  # GET /audiovisuals/1.json
  def show
    @audiovisual = Audiovisual.find(params[:id])
  end

  # GET /audiovisuals/new
  def new
    @audiovisual = Audiovisual.new
  end

  # GET /audiovisuals/1/edit
  def edit
    @audiovisual = Audiovisual.find(params[:id])
  end

  # POST /audiovisuals
  # POST /audiovisuals.json
  def create
    @audiovisual = Audiovisual.new(audiovisual_params)

    respond_to do |format|
      if @audiovisual.save
        format.html { redirect_to '/presupuestos', notice: 'SU SOLICITUD PARA PRESUPUESTO DE MARKETING AUDIOVISUAL HA SIDO ENVIADA.' }
        format.json { render :show, status: :created, location: @audiovisual }
        BudgetMailer.budget_email(audiovisual_params, "Marketing Audiovisual", "http://localhost:3000/admin/audiovisuals/#{Audiovisual.last.id}").deliver_now
      else
        format.html { render :new }
        format.json { render json: @audiovisual.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /audiovisuals/1
  # PATCH/PUT /audiovisuals/1.json
  def update
    respond_to do |format|
      if @audiovisual.update(audiovisual_params)
        format.html { redirect_to @audiovisual, notice: 'Audiovisual was successfully updated.' }
        format.json { render :show, status: :ok, location: @audiovisual }
      else
        format.html { render :edit }
        format.json { render json: @audiovisual.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /audiovisuals/1
  # DELETE /audiovisuals/1.json
  def destroy
    @audiovisual.destroy
    respond_to do |format|
      format.html { redirect_to "/admin/audiovisuals", notice: 'Audiovisual was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_audiovisual
      @audiovisual = Audiovisual.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def audiovisual_params
      params.require(:audiovisual).permit(:name,:telephone,:email,:brand,:editable_logo,:audio_video_hq,:photomontage,:photoshoot_private,:photo_edit_public,:photo_edit_private,:digital_makeover,:photobook,:wedding,:corp_event,:fifteen,:birthday,:photo_print,:other_photo,:shortfilm,:tv_spot,:web_spot,:video_report,:videoclip,:video_bio,:loop_screens,:wedding_video,:fifteen_video,:corp_event_video,:documentary,:promo,:other_video,:animation_2d,:animation_3d,:arquitecture,:interior_design,:tour_3d,:logo_3d,:promo_animation,:loop_screens_animation,:web_banner,:other_animation,:visual_concept,:marketing_strategy,:community_manager,:campaing,:street_marketing,:ambient_marketing,:brand_test,:focus_group,:strategy_media,:other_marketing,:story_board,:tv_script,:video_production,:direction,:brand_slogan,:product_slogan,:visual_identity,:jingle_lyrics,:mention_text,:supervision,:radio_jingle,:other_other,:project,:observations,:impresion_check,:other_photo_check,:other_animation_check,:other_marketing_check,:other_other_check,:animation_2d_check,:animation_3d_check, :radio_jingle_check,:shortfilm_check,:tv_spot_check,:web_spot_check,:video_report_check,:videoclip_check,:video_bio_check,:loop_screens_check,:wedding_video_check,:fifteen_video_check,:corp_event_check,:documentary_check,:promo_check,:other_video_check)
    end
    def resolve_layout
      case action_name
      when "destroy", "update", "edit", "show", "index"
        'admin/application'
      else
        'frontend/application'
      end
    end
end
