class FrontendController < ApplicationController
	layout 'layouts/frontend/application'

	def index
		Location.add(request.remote_ip)

		@produccion= InyxCatalogRails::Catalog.where(category: "PRODUCCIÓN DE EVENTOS").first
		@branding= InyxCatalogRails::Catalog.where(category: "BRANDING").first
		@marketing= InyxCatalogRails::Catalog.where(category: "MARKETING AUDIOVISUAL").first
	end

	def service
		@catalogo = InyxCatalogRails::Catalog.where(category: (params[:category].gsub("-", " ").mb_chars.upcase))
		@attachments = @catalogo.find_by_permalink(params[:permalink]).attachments.where(public: true).order(:created_at).reverse_order

		if @catalogo.first.category ==  "PRODUCCIÓN DE EVENTOS"
			@text = "Promovemos la innovación contando con el mejor talento humano que convertirá tu evento en una experiencia inolvidable."
		elsif @catalogo.first.category ==  "BRANDING"
			@text = "Resaltamos el poder de tu marca utilizando estrategias integrales que fortalecen tu imagen y te diferencian en el mercado."
		elsif @catalogo.first.category ==  "MARKETING AUDIOVISUAL"
			@text = "Nuestro talentoso equipo quiere dar a conocer tus ideas, aprovechándolas al máximo y creando campañas atractivas a través de contenidos originales."
		elsif @catalogo.first.category ==  "DIVOZ"
			@text = "Únete a nuestros felices clientes y permítenos ser parte de tu historia."
		end
		
	end
	
	def presupuestos
		@brand = Brand.new
		@production_event = ProductionEvent.new
	end


end