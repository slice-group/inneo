class WebsController < ApplicationController
  before_action :set_web, only: [:show, :edit, :update, :destroy]
  layout :resolve_layout
  before_filter :authenticate_user!, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:new,:create]
  # GET /webs/1
  # GET /webs/1.json
  def show
    @web = Web.find(params[:id])
  end

  # GET /webs/new
  def new
    @web = Web.new
  end

  # GET /webs/1/edit
  def edit
    @web = Web.find(params[:id])
  end

  # POST /webs
  # POST /webs.json
  def create
    @web = Web.new(web_params)

    respond_to do |format|
      if @web.save
        format.html { redirect_to '/presupuestos', notice: 'SU SOLICITUD PARA PRESUPUESTO DE PÁGINA WEB HA SIDO ENVIADA.' }
        format.json { render :show, status: :created, location: @web }
        BudgetMailer.budget_email(web_params, "Páginas Web", "http://localhost:3000/admin/webs/#{Web.last.id}").deliver_now
      else
        format.html { render :new }
        format.json { render json: @web.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /webs/1
  # PATCH/PUT /webs/1.json
  def update
    respond_to do |format|
      if @web.update(web_params)
        format.html { redirect_to @web, notice: 'Web was successfully updated.' }
        format.json { render :show, status: :ok, location: @web }
      else
        format.html { render :edit }
        format.json { render json: @web.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /webs/1
  # DELETE /webs/1.json
  def destroy
    @web.destroy
    respond_to do |format|
      format.html { redirect_to '/admin/webs', notice: 'Web was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_web
      @web = Web.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def web_params
      params.require(:web).permit( :name, :email, :telephone, :brand, :brand_description, :brand_target, :editable_logo, :audio_video_hq, :photo, :audio, :video, :animation, :socials, :other_content, :blog, :catalog_products, :catalog_gallery, :catalog_portfolio, :e_ccommerce, :payment, :online_classes, :online_chat, :online_reservation, :polls, :streaming_audio, :corp_email, :google_apps, :marketing_strategy, :marketing_manager, :graphic_design, :tv_publicity, :marketing_email, :benchmarking, :url1, :url2, :url3, :observations, :other_content_check)
    end

    def resolve_layout
      case action_name
      when "destroy", "update", "edit", "show", "index"
        'admin/application'
      else
        'frontend/application'
      end
    end
end
