class BudgetMailer < ApplicationMailer

	def budget_email(params, form, link )
		@params = params
		@form = form
		@link = link
		mail(to: "oscarlyncostero@gmail.com", subject: 'Nueva solicitud de Presupuesto')
	end
	
end
