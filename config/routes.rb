Rails.application.routes.draw do

  resources :production_events, only: [:new, :create]
  resources :brands, only: [:new, :create]
  resources :webs, only: [:new, :create]
  resources :audiovisuals, only: [:new, :create]


  root to: 'frontend#index'
  
  get '/service/:category/:permalink', to: 'frontend#service', as: 'service'
  get '/presupuestos', to: 'frontend#presupuestos', as: 'presupuesto'

  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]

  mount InyxCatalogRails::Engine, :at => '', as: 'catalog'

  scope :admin do
    resources :production_events, skip: [:new, :create] do
      collection do
        post '/delete', to: 'production_events#delete'
      end
    end
    resources :brands, skip: [:new, :create] do
      collection do
        post '/delete', to: 'brands#delete'
      end
    end
    resources :webs, skip: [:new, :create] do
      collection do
        post '/delete', to: 'webs#delete'
      end
    end
    resources :audiovisuals, skip: [:new, :create] do
      collection do
        post '/delete', to: 'audiovisuals#delete'
      end
    end

  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end
end
