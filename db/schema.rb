# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150601200429) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "audiovisuals", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "telephone"
    t.string   "brand"
    t.boolean  "editable_logo"
    t.boolean  "audio_video_hq"
    t.boolean  "photomontage"
    t.boolean  "photoshoot_private"
    t.boolean  "photo_edit_public"
    t.boolean  "photo_edit_private"
    t.boolean  "digital_makeover"
    t.boolean  "photobook"
    t.boolean  "wedding"
    t.boolean  "corp_event"
    t.boolean  "fifteen"
    t.boolean  "birthday"
    t.string   "photo_print"
    t.string   "other_photo"
    t.string   "shortfilm"
    t.string   "tv_spot"
    t.string   "web_spot"
    t.string   "video_report"
    t.string   "videoclip"
    t.string   "video_bio"
    t.string   "loop_screens"
    t.string   "wedding_video"
    t.string   "fifteen_video"
    t.string   "corp_event_video"
    t.string   "documentary"
    t.string   "promo"
    t.string   "other_video"
    t.string   "animation_2d"
    t.string   "animation_3d"
    t.boolean  "arquitecture"
    t.boolean  "interior_design"
    t.boolean  "tour_3d"
    t.boolean  "logo_3d"
    t.boolean  "promo_animation"
    t.boolean  "loop_screens_animation"
    t.boolean  "web_banner"
    t.string   "other_animation"
    t.boolean  "visual_concept"
    t.boolean  "marketing_strategy"
    t.boolean  "community_manager"
    t.boolean  "campaing"
    t.boolean  "street_marketing"
    t.boolean  "ambient_marketing"
    t.boolean  "brand_test"
    t.boolean  "focus_group"
    t.boolean  "strategy_media"
    t.string   "other_marketing"
    t.boolean  "story_board"
    t.boolean  "tv_script"
    t.boolean  "video_production"
    t.boolean  "direction"
    t.boolean  "brand_slogan"
    t.boolean  "product_slogan"
    t.boolean  "visual_identity"
    t.boolean  "jingle_lyrics"
    t.boolean  "mention_text"
    t.boolean  "supervision"
    t.string   "radio_jingle"
    t.string   "other_other"
    t.text     "project"
    t.text     "observations"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "telephone"
    t.string   "brand_name"
    t.boolean  "logo"
    t.boolean  "tdp"
    t.boolean  "envelope"
    t.boolean  "folder"
    t.boolean  "email_signature"
    t.boolean  "products_menu"
    t.boolean  "brand_menu"
    t.boolean  "letterhead"
    t.boolean  "notebook"
    t.boolean  "carnet"
    t.boolean  "bill"
    t.boolean  "uniform"
    t.boolean  "brochure"
    t.string   "other_corp"
    t.boolean  "pens"
    t.boolean  "calendars"
    t.boolean  "mouse_pad"
    t.boolean  "disposable_ids"
    t.boolean  "resin_ids"
    t.boolean  "mugs"
    t.boolean  "coolers"
    t.boolean  "key_chain"
    t.boolean  "hats"
    t.boolean  "t_shirts"
    t.boolean  "stickers"
    t.string   "other_pop"
    t.boolean  "poster_half_a4"
    t.boolean  "poster_quarter_a4"
    t.boolean  "poster"
    t.boolean  "press_full"
    t.boolean  "press_double"
    t.boolean  "press_half"
    t.boolean  "press_quarter"
    t.boolean  "press_eights"
    t.boolean  "pennon"
    t.boolean  "banner"
    t.string   "backing"
    t.string   "other_printed"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "inyx_catalog_rails_attachments", force: :cascade do |t|
    t.string   "name"
    t.string   "upload"
    t.text     "description"
    t.string   "image"
    t.text     "url"
    t.text     "target"
    t.integer  "catalog_id"
    t.boolean  "public"
    t.string   "permalink"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inyx_catalog_rails_catalogs", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "cover"
    t.string   "category"
    t.boolean  "public"
    t.string   "permalink"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "ip"
    t.string   "country_code"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "production_events", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "telephone"
    t.string   "event_type"
    t.string   "place"
    t.string   "number_guests"
    t.string   "hour_begin"
    t.string   "hour_end"
    t.date     "date_event"
    t.string   "mode"
    t.boolean  "sound"
    t.boolean  "video"
    t.boolean  "confetti"
    t.boolean  "lights"
    t.boolean  "sparkles"
    t.boolean  "led_robot"
    t.boolean  "led_screens"
    t.boolean  "fireworks"
    t.boolean  "strippers"
    t.boolean  "photoshoot"
    t.boolean  "garotas"
    t.boolean  "stilts"
    t.string   "other_service"
    t.boolean  "strings"
    t.boolean  "drums"
    t.boolean  "mariachi"
    t.boolean  "vallenato"
    t.string   "other_group"
    t.text     "comment"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "permalink"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "webs", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "telephone"
    t.text     "brand_description"
    t.text     "brand_target"
    t.boolean  "editable_logo"
    t.boolean  "audio_video_hq"
    t.boolean  "photo"
    t.boolean  "audio"
    t.boolean  "video"
    t.boolean  "animation"
    t.boolean  "socials"
    t.string   "other_content"
    t.boolean  "blog"
    t.boolean  "catalog_products"
    t.boolean  "catalog_gallery"
    t.boolean  "catalog_portfolio"
    t.boolean  "e_ccommerce"
    t.boolean  "payment"
    t.boolean  "online_classes"
    t.boolean  "online_chat"
    t.boolean  "online_reservation"
    t.boolean  "polls"
    t.boolean  "streaming_audio"
    t.boolean  "corp_email"
    t.boolean  "google_apps"
    t.boolean  "marketing_strategy"
    t.boolean  "marketing_manager"
    t.boolean  "graphic_design"
    t.boolean  "tv_publicity"
    t.boolean  "marketing_email"
    t.boolean  "benchmarking"
    t.string   "url1"
    t.string   "url2"
    t.string   "url3"
    t.text     "observations"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "brand"
  end

end
