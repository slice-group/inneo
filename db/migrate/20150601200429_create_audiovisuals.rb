class CreateAudiovisuals < ActiveRecord::Migration
  def change
    create_table :audiovisuals do |t|

    	t.string :name
      t.string :email
      t.string :telephone
      t.string :brand

      t.boolean :editable_logo
      t.boolean :audio_video_hq

      #Fotografia
      t.boolean :photomontage
      t.boolean :photoshoot_private
      t.boolean :photo_edit_public
      t.boolean :photo_edit_private
      t.boolean :digital_makeover
      t.boolean :photobook
      t.boolean :wedding
      t.boolean :corp_event
      t.boolean :fifteen
      t.boolean :birthday
      t.string :photo_print
      t.string :other_photo

      #Video
      t.string :shortfilm
      t.string :tv_spot
      t.string :web_spot
      t.string :video_report
      t.string :videoclip
      t.string :video_bio
      t.string :loop_screens
      t.string :wedding_video
      t.string :fifteen_video
      t.string :corp_event_video
      t.string :documentary
      t.string :promo
      t.string :other_video

      #Animacion
      t.string :animation_2d
      t.string :animation_3d
      t.boolean :arquitecture
      t.boolean :interior_design
      t.boolean :tour_3d
      t.boolean :logo_3d
      t.boolean :promo_animation
      t.boolean :loop_screens_animation
      t.boolean :web_banner
      t.string :other_animation

      #Marketing
      t.boolean :visual_concept
      t.boolean :marketing_strategy
      t.boolean :community_manager
      t.boolean :campaing
      t.boolean :street_marketing
      t.boolean :ambient_marketing
      t.boolean :brand_test
      t.boolean :focus_group
      t.boolean :strategy_media
      t.string :other_marketing

      #Otros
      t.boolean :story_board
      t.boolean :tv_script
      t.boolean :video_production
      t.boolean :direction
      t.boolean :brand_slogan
      t.boolean :product_slogan
      t.boolean :visual_identity
      t.boolean :jingle_lyrics
      t.boolean :mention_text
      t.boolean :supervision
      t.string :radio_jingle
      t.string :other_other

      t.text :project
      t.text :observations
    	
      t.timestamps null: false
    end
  end
end
