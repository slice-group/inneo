class CreateProductionEvents < ActiveRecord::Migration
  def change
    create_table :production_events do |t|

    	t.string :name
    	t.string :email
    	t.string :telephone

    	t.string :event_type
    	t.string :place
    	t.string :number_guests
    	t.string :hour_begin
    	t.string :hour_end
        t.date :date_event

    	t.string :mode
    	#services
    	t.boolean :sound
    	t.boolean :video
    	t.boolean :confetti
    	t.boolean :lights
    	t.boolean :sparkles
    	t.boolean :led_robot
    	t.boolean :led_screens
    	t.boolean :fireworks
    	t.boolean :strippers
    	t.boolean :photoshoot
    	t.boolean :garotas
    	t.boolean :stilts
    	t.string :other_service
    	#music groups
    	t.boolean :strings
    	t.boolean :drums
    	t.boolean :mariachi
    	t.boolean :vallenato
    	t.string :other_group

    	t.text :comment

      t.timestamps null: false
    end
  end
end

 