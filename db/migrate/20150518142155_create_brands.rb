class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|

      t.string :name
      t.string :email
      t.string :telephone
      t.string :brand_name

      #Identidad Corporativa
      t.boolean :logo
      t.boolean :tdp
      t.boolean :envelope
      t.boolean :folder
      t.boolean :email_signature
      t.boolean :products_menu
      t.boolean :brand_menu
      t.boolean :letterhead
      t.boolean :notebook
      t.boolean :carnet
      t.boolean :bill
      t.boolean :uniform
      t.boolean :brochure
      t.string :other_corp

      #Material P.O.P
      t.boolean :pens
      t.boolean :calendars
      t.boolean :mouse_pad
      t.boolean :disposable_ids
      t.boolean :resin_ids
      t.boolean :mugs
      t.boolean :coolers
      t.boolean :key_chain
      t.boolean :hats
      t.boolean :t_shirts
      t.boolean :stickers
      t.string :other_pop

      #Material Impreso
      t.boolean :poster_half_a4
      t.boolean :poster_quarter_a4
      t.boolean :poster
      t.boolean :press_full
      t.boolean :press_double
      t.boolean :press_half
      t.boolean :press_quarter
      t.boolean :press_eights
      t.boolean :pennon
      t.boolean :banner
      t.string :backing
      t.string :other_printed

      t.timestamps null: false
    end
  end
end
