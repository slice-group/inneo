class CreateWebs < ActiveRecord::Migration
  def change
    create_table :webs do |t|

    	t.string :name
      t.string :email
      t.string :telephone
      t.text :brand_description
      t.text :brand_target

      t.boolean :editable_logo
      t.boolean :audio_video_hq
      t.boolean :photo
      t.boolean :audio
      t.boolean :video
      t.boolean :animation
      t.boolean :socials
      t.string :other_content

      #Caracteristicas de la web
      t.boolean :blog
      t.boolean :catalog_products
      t.boolean :catalog_gallery
      t.boolean :catalog_portfolio
      t.boolean :e_ccommerce
      t.boolean :payment
      t.boolean :online_classes
      t.boolean :online_chat
      t.boolean :online_reservation
      t.boolean :polls
      t.boolean :streaming_audio
      t.boolean :corp_email
      t.boolean :google_apps
      t.boolean :marketing_strategy
      t.boolean :marketing_manager
      t.boolean :graphic_design
      t.boolean :tv_publicity
      t.boolean :marketing_email
      t.boolean :benchmarking

     	t.string :url1
     	t.string :url2
     	t.string :url3

     	t.text :observations

      t.timestamps null: false
    end
  end
end
